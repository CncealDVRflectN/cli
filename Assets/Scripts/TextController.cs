﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    [SerializeField] private Text _text;

    private void Awake()
    {
        _text.text =
#if TEST_1
            "TEST_1";
#elif TEST_2
            "TEST_2";
#else
            "common";
#endif
    }
}
