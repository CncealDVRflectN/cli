﻿using UnityEngine;
using Orion.Cli;

[CliOptionHandler("test2")]
public class Test2CliOption : CliOptionHandler
{
    protected override void Execute(ICliCommandContext context)
    {
        base.Execute(context);

        Debug.Log("Enable test 2...");
        CliUtils.SetTest1();
    }
}
