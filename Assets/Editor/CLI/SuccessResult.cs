﻿using Orion.Cli;
using System;
using UnityEngine;

[CliOptionHandler("success-result")]
public class SuccessResult : CliOptionHandler
{
    protected override void Execute(ICliCommandContext context)
    {
        base.Execute(context);

        Debug.Log("[DEBUG] SuccessResult");
    }
}

[CliOptionHandler("failed-result")]
public class FailedResult : CliOptionHandler
{
    protected override void Execute(ICliCommandContext context)
    {
        base.Execute(context);

        Debug.Log("[DEBUG] FailedResult");
        throw new Exception("FailedResult");
    }
}
