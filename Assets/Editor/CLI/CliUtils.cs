﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Compilation;

public static class CliUtils
{
    public static void SetTest1()
    {
        BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
        
        Debug.Log($"Set TEST_1 for '{buildTargetGroup}'");
        PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, "TEST_1");
        CompilationPipeline.RequestScriptCompilation();
        EditorUtility.RequestScriptReload();
    }

    public static void SetTest2()
    {
        BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;

        Debug.Log($"Set TEST_2 for '{buildTargetGroup}'");
        PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, "TEST_2");
        CompilationPipeline.RequestScriptCompilation();
        EditorUtility.RequestScriptReload();
    }
}
