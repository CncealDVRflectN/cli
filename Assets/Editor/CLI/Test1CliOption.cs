﻿using UnityEngine;
using Orion.Cli;

[CliOptionHandler("test1")]
public class Test1CliOption : CliOptionHandler
{
    protected override void Execute(ICliCommandContext context)
    {
        base.Execute(context);

        Debug.Log("Enable test 1...");
        CliUtils.SetTest1();
    }
}
