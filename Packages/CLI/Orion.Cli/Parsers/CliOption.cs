﻿using System.Collections.Generic;

namespace Orion.Cli.Parsing
{
    public sealed class CliOption : ICliOption
    {
        private List<CliValue> _values;

        public string Name { get; }

        public IReadOnlyList<ICliValue> Values => _values;

        public bool IsEmpty => _values == null || _values.Count == 0;

        public bool IsCollection => _values != null && _values.Count > 1;

        public CliOption(string name)
        {
            Name = name;
        }

        public void AddValue(CliValue value)
        {
            if (_values == null)
                _values = new List<CliValue>();

            _values.Add(value);
        }
    }
}
