﻿namespace Orion.Cli.Parsing
{
    public class CliValue : ICliValue
    {
        public string RawData { get; }

        public CliValue(string rawData)
        {
            RawData = rawData;
        }
    }
}
