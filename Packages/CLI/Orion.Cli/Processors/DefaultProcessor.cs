﻿using Orion.Cli.Contracts;
using Orion.Cli.Parsing;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Orion.Cli.Processing
{
    public class DefaultProcessor : CliProcessor
    {
        private ICliOptionPropertiesContract _optionPropertiesContract;

        public DefaultProcessor()
        {
            Parser = new DefaultParser();
            OptionHandlersResolver = new DefaultOptionHandlersResolver();
            _optionPropertiesContract = new DefaultOptionPropertiesContract();
        }

        public override async Task ProcessAsync(string[] commandLineArgs, CancellationToken cancellationToken)
        {
            var command = Parser.ParseCommandLineArgs(commandLineArgs);
            var handler = new CliCommandProcessor(command, OptionHandlersResolver, _optionPropertiesContract);

            if (Application.isBatchMode)
            {
                try
                {
                    await handler.ProcessAsync(cancellationToken);
                    EditorApplication.Exit(0);
                }
                catch (Exception e)
                {
                    var sb = new StringBuilder();
                    sb.AppendLine("================================================================================");
                    sb.AppendLine("Orion CLI");
                    sb.AppendLine("---------");
                    sb.AppendLine();
                    sb.AppendLine("Execution failed due to exception:");
                    sb.AppendLine(e.ToString());
                    sb.AppendLine("================================================================================");

                    Debug.Log(sb.ToString());
                    EditorApplication.Exit(1); // 'throw e' doesn't close unity in batchmode
                    throw e;
                }
            }
            else
                await handler.ProcessAsync(cancellationToken);
        }
    }
}
