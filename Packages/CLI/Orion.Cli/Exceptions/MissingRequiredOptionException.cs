﻿using System;

namespace Orion.Cli
{
    public class MissingRequiredOptionException : Exception
    {
        public MissingRequiredOptionException(string name) : base($"Required option with name '{name}' doesn't exist.")
        {
        }
    }
}
