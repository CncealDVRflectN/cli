﻿using System;

namespace Orion.Cli
{
    public interface IValueConverter
    {
        bool CanConvert(Type type);
        object GetValue(ICliValue value, Type propertyType);
    }
}
