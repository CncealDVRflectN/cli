﻿namespace Orion.Cli
{
    public enum CliCommandStatus
    {
        None = 0,
        InProgress,
        Succeeded,
        Failed,
    }
}
