﻿using System;

namespace Orion.Cli
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class CliOptionHandlerAttribute : Attribute
    {
        /// <summary>
        /// The option name to handle.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Execution order.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// The priority among multiple handlers with the same option name.
        /// Might be useful with <see cref="IsFallback"/>.
        /// By default priority equals to <see cref="Order"/>.
        /// </summary>
        public int? Priority { get; set; }

        /// <summary>
        /// Determine whether the handler is a fallback.
        /// </summary>
        public bool IsFallback { get; set; }


        public CliOptionHandlerAttribute(string name)
        {
            Name = name;
        }
    }
}
