﻿using System;

namespace Orion.Cli
{
    public interface ICliParser
    {
        ICliCommand ParseCommandLineArgs(params string[] args);
    }


    public static class CliParserExtensions
    {
        public static ICliCommand ParseCommandLineArgs(this ICliParser parser)
            => parser.ParseCommandLineArgs(Environment.GetCommandLineArgs());
    }
}
