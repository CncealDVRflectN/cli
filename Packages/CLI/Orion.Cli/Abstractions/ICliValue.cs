﻿using System;

namespace Orion.Cli
{
    public interface ICliValue
    {
        string RawData { get; }
    }
}
