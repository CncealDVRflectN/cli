﻿using System.Collections.Generic;

namespace Orion.Cli
{
    public interface ICliOptionHandlersResolver
    {
        IReadOnlyList<CliOptionHandler> ResolveOptionHandlers(IEnumerable<ICliOption> options);
    }
}
