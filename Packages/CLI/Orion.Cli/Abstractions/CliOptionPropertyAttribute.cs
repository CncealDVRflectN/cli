﻿using System;

namespace Orion.Cli
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class CliOptionPropertyAttribute : Attribute
    {
        public string Name { get; }
        public bool Required { get; set; }

        public CliOptionPropertyAttribute()
        {
        }

        public CliOptionPropertyAttribute(string name)
        {
            Name = name;
        }
    }
}
