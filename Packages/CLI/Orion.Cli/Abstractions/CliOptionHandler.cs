﻿using System.Threading;
using System.Threading.Tasks;

namespace Orion.Cli
{
    public abstract class CliOptionHandler
    {
        protected internal virtual void Initialize(ICliCommandContext context) { }
        protected internal virtual Task InitializeAsync(ICliCommandContext context, CancellationToken cancellationToken) => Task.CompletedTask;
        protected internal virtual void Prepare(ICliCommandContext context) { }
        protected internal virtual Task PrepareAsync(ICliCommandContext context, CancellationToken cancellationToken) => Task.CompletedTask;
        protected internal virtual void Execute(ICliCommandContext context) { }
        protected internal virtual Task ExecuteAsync(ICliCommandContext context, CancellationToken cancellationToken) => Task.CompletedTask;
        protected internal virtual void Release(ICliCommandContext context) { }
        protected internal virtual Task ReleaseAsync(ICliCommandContext context, CancellationToken cancellationToken) => Task.CompletedTask;

        internal Task InitializeInternalAsync(ICliCommandContext context, CancellationToken cancellationToken)
        {
            Initialize(context);
            return InitializeAsync(context, cancellationToken);
        }

        internal Task PrepareInternalAsync(ICliCommandContext context, CancellationToken cancellationToken)
        {
            Prepare(context);
            return PrepareAsync(context, cancellationToken);
        }

        internal Task ExecuteInternalAsync(ICliCommandContext context, CancellationToken cancellationToken)
        {
            Execute(context);
            return ExecuteAsync(context, cancellationToken);
        }

        internal Task ReleaseInternalAsync(ICliCommandContext context, CancellationToken cancellationToken)
        {
            Release(context);
            return ReleaseAsync(context, cancellationToken);
        }
    }
}
