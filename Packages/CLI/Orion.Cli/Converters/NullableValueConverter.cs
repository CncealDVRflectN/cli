﻿using System;

namespace Orion.Cli
{
    namespace Converters
    {
        public abstract class NullableValueConverter<T> : IValueConverter
        {
            public virtual bool CanConvert(Type type) => type == typeof(T)
                || (type.IsGenericType
                    && type.GetGenericTypeDefinition() == typeof(Nullable<>)
                    && type.GenericTypeArguments[0] == typeof(T));

            public virtual object GetValue(ICliValue value, Type propertyType)
            {
                bool isNullable = propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>);
                if (isNullable) propertyType = propertyType.GenericTypeArguments[0];

                return isNullable && (value == null || string.IsNullOrEmpty(value.RawData))
                    ? null
                    : GetValueInternal(value, propertyType);
            }

            protected abstract object GetValueInternal(ICliValue value, Type propertyType);
        }
    }
}
