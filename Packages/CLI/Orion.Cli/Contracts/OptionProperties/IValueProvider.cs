﻿using System;

namespace Orion.Cli.Contracts
{
    public interface IValueProvider
    {
        Type Type { get; }

        object GetValue(object obj);
        void SetValue(object obj, object value);
    }
}
