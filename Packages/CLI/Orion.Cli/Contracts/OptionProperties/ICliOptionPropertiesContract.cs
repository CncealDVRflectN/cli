﻿using System.Collections.Generic;

namespace Orion.Cli.Contracts
{
    internal interface ICliOptionPropertiesContract
    {
        void PopulateObject(object obj, IReadOnlyList<ICliOption> options);
    }
}
