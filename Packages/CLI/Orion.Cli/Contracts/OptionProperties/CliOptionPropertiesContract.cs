﻿using Orion.Cli.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Orion.Cli.Contracts
{
    // it is internal class while work in progress
    internal class DefaultOptionPropertiesContract : ICliOptionPropertiesContract
    {
        private List<IValueConverter> _valueConverters;

        public DefaultOptionPropertiesContract()
        {
            _valueConverters = new List<IValueConverter>();
            _valueConverters.Add(new BooleanValueConverter());
            _valueConverters.Add(new ByteValueConverter());
            _valueConverters.Add(new Int32ValueConverter());
            _valueConverters.Add(new UInt32ValueConverter());
            _valueConverters.Add(new Int64ValueConverter());
            _valueConverters.Add(new UInt64ValueConverter());
            _valueConverters.Add(new FloatValueConverter());
            _valueConverters.Add(new DoubleValueConverter());
            _valueConverters.Add(new StringValueConverter());
            _valueConverters.Add(new EnumValueConverter());

            // TODO: custom converters
        }

        private IReadOnlyList<CliOptionProperty> ResolveProperties(Type handlerType)
        {
            var properties = new List<CliOptionProperty>();
            foreach (var member in GetMembers(handlerType))
            {
                var attribute = member.GetCustomAttribute<CliOptionPropertyAttribute>();
                if (attribute == null) continue;

                var property = new CliOptionProperty();
                property.Required = attribute.Required;
                property.Name = ResolvePropertyName(member, attribute);
                property.ValueProvider = ResolveValueProvider(member);
                property.ValueConverter = ResolveValueConverter(property.ValueProvider.Type);

                if (property.IsArray)
                    property.ItemValueConverter = ResolveValueConverter(property.PropertyType.GetElementType());
                else if (property.IsCollection)
                    property.ItemValueConverter = ResolveValueConverter(property.PropertyType.GetGenericArguments()[0]);

                properties.Add(property);
            }

            return properties;
        }

        private IEnumerable<MemberInfo> GetMembers(Type type)
        {
            while (type != null)
            {
                var members = type.GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (var member in members)
                    yield return member;

                type = type.BaseType;
            }

        }

        private string ResolvePropertyName(MemberInfo memberInfo, CliOptionPropertyAttribute attribute)
        {
            var name = attribute.Name;

            if (string.IsNullOrEmpty(name))
                name = memberInfo.Name
                    .Replace("-", "")
                    .TrimStart('_');

            return name;
        }

        private IValueProvider ResolveValueProvider(MemberInfo member)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Field: return new FieldValueProvider(member as FieldInfo);
                case MemberTypes.Property: return new PropertyValueProvider(member as PropertyInfo);
                default:
                    throw new NotImplementedException();
            }
        }

        private IValueConverter ResolveValueConverter(Type propertyType)
        {
            return _valueConverters.FirstOrDefault(p => p.CanConvert(propertyType));
        }


        public void PopulateObject(object obj, IReadOnlyList<ICliOption> options)
        {
            if (options == null || options.Count == 0) return;

            var properties = ResolveProperties(obj.GetType());
            foreach (var property in properties)
            {
                if (property.IsArray) PopulateArray(obj, property, options);
                else if (property.IsCollection) PopulateCollection(obj, property, options);
                else PopulateValue(obj, property, options);
            }
        }

        private void PopulateArray(object obj, CliOptionProperty property, IEnumerable<ICliOption> options)
        {
            var optionsCount = options.Count(p => string.Equals(p.Name, property.Name, StringComparison.OrdinalIgnoreCase));
            if (optionsCount == 0)
            {
                CheckRequiredProperty(property);
                return;
            }

            var cliValues = options
                .Where(p => string.Equals(p.Name, property.Name, StringComparison.OrdinalIgnoreCase))
                .Where(p => p.Values != null)
                .SelectMany(p => p.Values)
                .ToList();

            var itemType = property.GetItemType();
            var array = Array.CreateInstance(itemType, cliValues.Count);
            for (int i = 0; i < cliValues.Count; i++)
            {
                var value = GetValue(property.ItemValueConverter, cliValues[i], itemType);
                array.SetValue(value, i);
            }

            property.ValueProvider.SetValue(obj, array);
        }

        private void PopulateCollection(object obj, CliOptionProperty property, IEnumerable<ICliOption> options)
        {
            var optionsCount = options.Count(p => string.Equals(p.Name, property.Name, StringComparison.OrdinalIgnoreCase));
            if (optionsCount == 0)
            {
                CheckRequiredProperty(property);
                return;
            }

            var cliValues = options
                .Where(p => string.Equals(p.Name, property.Name, StringComparison.OrdinalIgnoreCase))
                .Where(p => p.Values != null)
                .SelectMany(p => p.Values)
                .ToList();

            var itemType = property.GetItemType();
            var collection = Activator.CreateInstance(property.PropertyType);
            var mi = property.PropertyType.GetMethod("Add");
            
            for (int i = 0; i < cliValues.Count; i++)
            {
                var value = GetValue(property.ItemValueConverter, cliValues[i], itemType);
                mi.Invoke(collection, new object[] { value });
            }

            property.ValueProvider.SetValue(obj, collection);
        }

        private void PopulateValue(object obj, CliOptionProperty property, IEnumerable<ICliOption> options)
        {
            // TODO: first/last
            var option = options.FirstOrDefault(p => string.Equals(p.Name, property.Name, StringComparison.OrdinalIgnoreCase));
            if (option == null)
            {
                CheckRequiredProperty(property);
                return;
            }

            // throw if option has multiple values
            int valuesCount = (option.Values?.Count).GetValueOrDefault(0);
            if (valuesCount > 1)
                throw new FormatException($"Option '{option.Name}' doesn't allow multiple values.");

            var cliValue = valuesCount == 1
                ? option.Values[0]
                : null;

            var value = GetValue(property.ValueConverter, cliValue, property.PropertyType);
            property.ValueProvider.SetValue(obj, value);
        }

        private object GetValue(IValueConverter converter, ICliValue value, Type type)
            => converter != null
                ? converter.GetValue(value, type)
                : Convert.ChangeType(value.RawData, type);

        private void CheckRequiredProperty(CliOptionProperty property)
        {
            // TODO: message from attribute
            if (property.Required) throw new MissingRequiredOptionException(property.Name);
        }


        class CliOptionProperty
        {
            private bool? _isCollection;

            public string Name { get; set; }
            public bool Required { get; set; }
            public IValueProvider ValueProvider { get; internal set; }
            public IValueConverter ValueConverter { get; internal set; }
            public IValueConverter ItemValueConverter { get; internal set; }
            public Type PropertyType => ValueProvider.Type;
            public bool IsArray => PropertyType.IsArray;
            public bool IsCollection => _isCollection.HasValue 
                ? _isCollection.Value 
                : (_isCollection = IsCollectionType(PropertyType)).Value;

            public Type GetItemType()
            {
                if (IsArray) return PropertyType.GetElementType();
                if (IsCollection) return PropertyType.GetGenericArguments()[0];
                return null;
            }

            private bool IsCollectionType(Type propertyType)
            {
                if (!propertyType.IsConstructedGenericType)
                    return false;

                foreach (var type in propertyType.GetInterfaces())
                {
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(ICollection<>)) return true;
                }

                return false;
            }
        }
    }
}
