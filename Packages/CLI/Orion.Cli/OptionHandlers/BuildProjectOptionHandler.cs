﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace Orion.Cli.OptionHandlers
{
    [CliOptionHandler("build-project", Order = CliUtils.BuildinOptionHandlersOrder, IsFallback = true)]
    public class BuildProjectOptionHandler : UnityOptionHandler
    {
        [CliOptionProperty] public string ProjectName { get; private set; }
        [CliOptionProperty] public string ProjectNameSuffix { get; private set; }
        [CliOptionProperty] public string ProjectVersion { get; private set; }
        [CliOptionProperty] public string ProjectVersionSuffix { get; private set; }
        [CliOptionProperty] public int? BuildNumber { get; private set; }

        [CliOptionProperty] public string BuildsRootPath { get; private set; } = "Builds";
        [CliOptionProperty] public string BuildReportFile { get; private set; }
        [CliOptionProperty] public AndroidExportMode AndroidExportMode { get; private set; } = AndroidExportMode.Apk;
        [CliOptionProperty] public string AppleDeveloperTeamId { get; private set; }

        [CliOptionProperty] public bool EnableDevelopmentBuild { get; private set; }
        [CliOptionProperty] public bool EnableScriptDebugging { get; private set; }
        [CliOptionProperty] public bool EnableDeepProfilingSupport { get; private set; }
        [CliOptionProperty] public bool EnableWaitingForDebugger { get; private set; }
        [CliOptionProperty] public bool CleanBuildCache { get; private set; }
        [CliOptionProperty] public string CompressionMethod { get; private set; }

        [CliOptionProperty] public string KeystoreCredentialsFile { get; private set; }
        [CliOptionProperty] public string KeystorePath { get; private set; }
        [CliOptionProperty] public string KeystorePassword { get; private set; }
        [CliOptionProperty] public string KeyAlias { get; private set; }
        [CliOptionProperty] public string KeyPassword { get; private set; }


        protected internal override void Initialize(ICliCommandContext context)
        {
            base.Initialize(context);

            AssertProjectPathDefined();
            AssertBuildTargetDefined();

            if (string.IsNullOrEmpty(ProjectName)) ProjectName = PlayerSettings.productName;
            if (string.IsNullOrEmpty(ProjectVersion)) ProjectVersion = PlayerSettings.bundleVersion;

            var keystoreCredentialsFile = context.GetOption("keystoreCredentialsFile").GetStringOrDefault();
            if (!string.IsNullOrEmpty(keystoreCredentialsFile))
            {
                string json = File.ReadAllText(keystoreCredentialsFile);
                var credentials = JsonUtility.FromJson<KeystoreCredentialsModel>(json);

                if (!string.IsNullOrEmpty(credentials.keystorePassword)) KeystorePassword = credentials.keystorePassword;
                if (!string.IsNullOrEmpty(credentials.keyAlias)) KeyAlias = credentials.keyAlias;
                if (!string.IsNullOrEmpty(credentials.keyPassword)) KeyPassword = credentials.keyPassword;
            }
        }

        protected internal override void Prepare(ICliCommandContext context)
        {
            base.Prepare(context);

            if (!string.IsNullOrEmpty(ProjectVersion))
                PlayerSettings.bundleVersion = ProjectVersion;
            
            // depends on script debugging
            EditorUserBuildSettings.waitForManagedDebugger = EnableWaitingForDebugger;

            // android settings
            if (BuildTarget == BuildTarget.Android)
            {
                if (BuildNumber.HasValue)
                    PlayerSettings.Android.bundleVersionCode = BuildNumber.Value;

                switch (AndroidExportMode)
                {
                    case AndroidExportMode.Project:
                        EditorUserBuildSettings.exportAsGoogleAndroidProject = true;
                        break;

                    case AndroidExportMode.Apk:
                        EditorUserBuildSettings.exportAsGoogleAndroidProject = false;
                        EditorUserBuildSettings.buildAppBundle = false;
                        break;

                    case AndroidExportMode.Aab:
                        EditorUserBuildSettings.exportAsGoogleAndroidProject = false;
                        EditorUserBuildSettings.buildAppBundle = true;
                        break;
                }

                // setup keystore
                if (!string.IsNullOrEmpty(KeystorePath))
                {
                    PlayerSettings.Android.keystoreName = KeystorePath;
#if UNITY_2019_1_OR_NEWER
                    PlayerSettings.Android.useCustomKeystore = true;
#endif
                }

                if (!string.IsNullOrEmpty(KeystorePassword)) PlayerSettings.Android.keystorePass = KeystorePassword;
                if (!string.IsNullOrEmpty(KeyAlias)) PlayerSettings.Android.keyaliasName = KeyAlias;
                if (!string.IsNullOrEmpty(KeyPassword)) PlayerSettings.Android.keyaliasPass = KeyPassword;
            }

            // ios settings
            else if (BuildTarget == BuildTarget.iOS)
            {
                if (BuildNumber.HasValue)
                    PlayerSettings.iOS.buildNumber = BuildNumber.Value.ToString();

                if (!string.IsNullOrEmpty(AppleDeveloperTeamId))
                    PlayerSettings.iOS.appleDeveloperTeamID = AppleDeveloperTeamId;
            }
        }

        protected internal override void Execute(ICliCommandContext context)
        {
            base.Execute(context);

            // create build option
            var options = CreateBuildPlayerOptions();

            // build project
            var report = BuildPipeline.BuildPlayer(options);
            var success = report.summary.result == BuildResult.Succeeded && report.summary.totalErrors == 0;

            // report
            if (!string.IsNullOrEmpty(BuildReportFile))
                File.WriteAllText(BuildReportFile, CreateBuildReport(options, report));

            // throw errors
            if (!success) throw new BuildFailedException($"Build failed. Total errors = {report.summary.totalErrors}");
        }

        protected virtual BuildPlayerOptions CreateBuildPlayerOptions()
        {
            BuildPlayerOptions pbo = new BuildPlayerOptions();
            pbo.target = BuildTarget;
            if (pbo.targetGroup == BuildTargetGroup.Unknown)
                pbo.targetGroup = BuildPipeline.GetBuildTargetGroup(BuildTarget);

            pbo.locationPathName = GetOutputPath();
            pbo.scenes = GetScenes().ToArray();

            pbo.options = BuildOptions.StrictMode;
            if (EnableDevelopmentBuild) pbo.options |= BuildOptions.Development;
            if (EnableScriptDebugging) pbo.options |= BuildOptions.AllowDebugging;
            //if (ProfilerEnabled) options.options |= BuildOptions.ConnectWithProfiler;
            if (EnableDeepProfilingSupport) pbo.options |= BuildOptions.EnableDeepProfilingSupport;

#if UNITY_2021_1_OR_NEWER
            if (CleanBuildCache) pbo.options |= BuildOptions.CleanBuildCache;
#endif

            // compression
            pbo.options |= GetCompressionMethod();

            return pbo;
        }

        protected virtual string CreateBuildReport(BuildPlayerOptions options, BuildReport report)
        {
            var reportModel = new BuildReportModel(GetFormatterBuildName(), options, report);
            return JsonUtility.ToJson(reportModel, true);
        }

        protected virtual IEnumerable<string> GetScenes() => EditorBuildSettings.scenes
            .Where(p => p.enabled)
            .Select(p => p.path);

        protected virtual BuildOptions GetCompressionMethod()
        {
            if (string.IsNullOrEmpty(CompressionMethod)) return 0;

            switch (CompressionMethod.ToLower())
            {
                case "lz4": return BuildOptions.CompressWithLz4;
                case "lz4hc": return BuildOptions.CompressWithLz4HC;
                default:
                    // EditorUserBuildSettings.GetCompressionType()
                    return 0;
            }
        }

        #region Name, Version, Path

        protected virtual string GetOutputPath()
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(BuildsRootPath))
            {
                sb.Append(BuildsRootPath);
                sb.Append('/');
            }

            sb.Append(GetFormatterBuildName());

            // android specific
            if (BuildTarget == BuildTarget.Android)
            {
                switch (AndroidExportMode)
                {
                    case AndroidExportMode.Apk: sb.Append(".apk"); break;
                    case AndroidExportMode.Aab: sb.Append(".aab"); break;
                }
            }

            return sb.ToString();
        }

        public string GetFormatterBuildName()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(GetFormattedProjectName());
            if (!string.IsNullOrEmpty(ProjectNameSuffix))
            {
                sb.Append('-');
                sb.Append(ProjectNameSuffix);
            }

            sb.Append('-');
            sb.Append(GetFormattedProjectVersion());
            if (!string.IsNullOrEmpty(ProjectVersionSuffix))
            {
                sb.Append('-');
                sb.Append(ProjectVersionSuffix);
            }

            if (EnableDevelopmentBuild)
                sb.Append("-dbg");

            return sb.ToString();
        }

        public string GetFormattedProjectName()
        {
            string name = Regex.Replace(ProjectName.Trim(), "[^.a-zA-Z0-9 -]", "")
                .ToLower()
                .Replace(' ', '-');

            if (string.IsNullOrEmpty(name))
                throw new FormatException($"Failed to get canonical project name form string '{ProjectName}'");

            return name;
        }

        public string GetFormattedProjectVersion()
        {
            string version = ProjectVersion.Trim();

            var match = Regex.Match(version, @"[\d.]+");
            if (!match.Success || match.Groups.Count != 1 || match.Length == 0)
                throw new FormatException($"Failed to parse project version string '{version}'");

            int idx1 = match.Index;
            int idx2 = match.Index + match.Length;

            var sb = new StringBuilder();
            if (idx1 > 0) sb.Append(version.Remove(idx1));
            sb.Append(version.Substring(idx1, idx2 - idx1).Trim('.'));

            if (BuildNumber.HasValue)
            {
                sb.Append('.');
                sb.Append(BuildNumber.Value);
            }

            if (idx2 < version.Length) sb.Append(version.Substring(idx2));

            return sb.ToString();
        }

        #endregion

        #region Data models
#pragma warning disable CS0649

        [Serializable]
        private class KeystoreCredentialsModel
        {
            public string keystorePassword;
            public string keyAlias;
            public string keyPassword;
        }

        [Serializable]
        private class BuildReportModel
        {
            public bool success;
            public string buildOutputPath;
            public string platform;
            public string platformGroup;
            public string applicationIdentifier;
            public string formattedBuildName;
            public IosBuildReportModel ios;

            public BuildReportModel(
                string formattedBuildName,
                BuildPlayerOptions options,
                BuildReport report)
            {
                success = report.summary.result == BuildResult.Succeeded && report.summary.totalErrors == 0;
                buildOutputPath = options.locationPathName;
                platform = report.summary.platform.ToString();
                platformGroup = report.summary.platformGroup.ToString();

                applicationIdentifier = PlayerSettings.GetApplicationIdentifier(report.summary.platformGroup);
                this.formattedBuildName = formattedBuildName;

                ios = new IosBuildReportModel(buildOutputPath);
            }
        }

        [Serializable]
        private class IosBuildReportModel
        {
            public string xcodeProjectPath;
            public string xcodeWorkspacePath;
            public string xcodeTarget;
            public string xcodeScheme;
            public string developerTeamId;

            public IosBuildReportModel(string buildOutputPath)
            {
                xcodeProjectPath = $"{buildOutputPath}/Unity-iPhone.xcodeproj";
                xcodeWorkspacePath = $"{buildOutputPath}/Unity-iPhone.xcworkspace";
                xcodeTarget = "Unity-iPhone";
                xcodeScheme = "Unity-iPhone";
                developerTeamId = PlayerSettings.iOS.appleDeveloperTeamID;
            }
        }

#pragma warning restore CS0649
        #endregion
    }

    public enum AndroidExportMode
    {
        Apk,
        Aab,
        Project,
    }
}
