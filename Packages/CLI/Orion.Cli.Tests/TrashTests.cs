﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Orion.Cli;
using Orion.Cli.Contracts;
using Orion.Cli.Processing;
using UnityEngine;
using UnityEngine.TestTools;

namespace Origin.Cli.Tests
{
    [TestFixture]
    public class TrashTests
    {
        [Test]
        public async Task Wip()
        {
            //string cmdline = "/Applications/Unity/Hub/Editor/2020.3.30f1/Unity.app/Contents/MacOS/Unity --build-player -batchmode -quit -accept-apiupdate -projectPath /Users/administrator/jenkins/workspace/decor-life/decor-life-android -buildTarget Android -username user -password passwd -logFile - --exportMode aab --keystorePath .ci/google-play.keystore --keystorePassword passwd --keyAlias upload --keyPassword passwd --buildNumber 83 --buildReportPath unity-build-report.json --prepare-for-build --versionsReport versions.log --dev-build --buildSuffix dev --ads-simulation enable --developmentBuild -executeMethod Orion.Cli.CommandLine.BuildProject";
            ////string cmdline = "/Applications/Unity/Hub/Editor/2020.3.30f1/Unity.app/Contents/MacOS/Unity --testOption value1 --testOption value2 --boolValue true --int32Value 123 --buildTarget android";
            ////string cmdline = "/Applications/Unity/Hub/Editor/2020.3.30f1/Unity.app/Contents/MacOS/Unity --testOption value1";

            string cmdline = "/Applications/Unity/Hub/Editor/2020.3.30f1/Unity.app/Contents/MacOS/Unity --testOption value1 --boolValue";
            //string cmdline = "/Applications/Unity/Hub/Editor/2020.3.30f1/Unity.app/Contents/MacOS/Unity --testOption value1 --boolValue --numbers";
            //string cmdline = "/Applications/Unity/Hub/Editor/2020.3.30f1/Unity.app/Contents/MacOS/Unity --testOption value1 --boolValue --numbers 1 2 3";
            var args = cmdline.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var processor = CliProcessor.CreateDefault();
            await processor.ProcessAsync(args, CancellationToken.None);
        }
    }


    [CliOptionHandler("testOption")]
    internal class TestOptionHandler : CliOptionHandler
    {
        [CliOptionProperty] public bool BoolValue { get; private set; }
        [CliOptionProperty] public string TestOption { get; private set; }
        //[CliOptionProperty] public int[] Numbers { get; private set; }
        [CliOptionProperty] public HashSet<int> Numbers { get; private set; }

        protected internal override async Task ExecuteAsync(ICliCommandContext context, CancellationToken cancellationToken)
        {
            TestContext.WriteLine($"BoolValue = {BoolValue};");
            TestContext.WriteLine($"testOption = {TestOption};");

            await Task.Delay(1000);

            if (Numbers != null)
            {
                TestContext.WriteLine($"Numbers NOT null");

                foreach (var num in Numbers)
                    TestContext.WriteLine($"num = {num};");
            }
        }
    }
}
