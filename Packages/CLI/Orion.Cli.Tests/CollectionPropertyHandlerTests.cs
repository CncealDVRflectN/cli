﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Orion.Cli;
using Orion.Cli.Contracts;
using Orion.Cli.Parsing;

namespace Origin.Cli.Tests
{
    [TestFixture]
    public class CollectionPropertyHandlerTests
    {
        [Test]
        public void PopulateObject_Collection_Null()
        {
            const string cmdline = "Unity";
            AssertPopulateCollection(cmdline, -1);
        }

        [Test]
        public void PopulateObject_Collection_Empty()
        {
            const string cmdline = "Unity --numbers";
            AssertPopulateCollection(cmdline, 0);
        }

        [Test]
        public void PopulateObject_Collection_OneItem()
        {
            const string cmdline = "Unity --numbers 1";
            AssertPopulateCollection(cmdline, 1);
        }

        [Test]
        public void PopulateObject_Collection_MultipleItems()
        {
            const string cmdline = "Unity --numbers 1 2 3";
            AssertPopulateCollection(cmdline, 3);
        }

        [Test]
        public void PopulateObject_TwoCollections_Empty()
        {
            const string cmdline = "Unity --numbers --numbers";
            AssertPopulateCollection(cmdline, 0);
        }

        [Test]
        public void PopulateObject_TwoCollections_OneItem()
        {
            const string cmdline = "Unity --numbers 1 --numbers";
            AssertPopulateCollection(cmdline, 1);
        }

        [Test]
        public void PopulateObject_TwoCollections_TwoItems()
        {
            const string cmdline = "Unity --numbers 1 --numbers 2";
            AssertPopulateCollection(cmdline, 2);
        }

        [Test]
        public void PopulateObject_TwoCollections_MultipleItems()
        {
            const string cmdline = "Unity --numbers 1 2 --numbers 3 4";
            AssertPopulateCollection(cmdline, 4);
        }


        private IReadOnlyList<ICliOption> GetOptions(string cmdline)
        {
            // parse
            var parser = new DefaultParser();
            var args = cmdline.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return parser.ParseCommandLineArgs(args).Options;
        }

        private void AssertPopulateCollection<T>(ICliOptionPropertiesContract contract, IReadOnlyList<ICliOption> options, int itemsCount)
            where T : TestOptionHandler, new()
        {
            var hander = new T();
            contract.PopulateObject(hander, options);

            var numbers = hander.GetNumbers();
            Assert.That(numbers, itemsCount < 0 ? Is.Null : Is.Not.Null);
            if (itemsCount < 0) return;

            Assert.That(numbers.Count, Is.EqualTo(itemsCount));

            for (int i = 0; i < itemsCount; i++)
                Assert.That(numbers.Contains(i + 1), Is.True);
        }

        private void AssertPopulateCollection(string cmdline, int itemsCount)
        {
            var options = GetOptions(cmdline);
            var contract = new DefaultOptionPropertiesContract();

            AssertPopulateCollection<ArrayOptionHandler>(contract, options, itemsCount);
            AssertPopulateCollection<HashSetOptionHandler>(contract, options, itemsCount);
        }


        private abstract class TestOptionHandler : CliOptionHandler
        {
            public abstract ICollection<int> GetNumbers();

            protected internal override void Execute(ICliCommandContext context)
            {
                var numbers = GetNumbers();
                if (numbers != null)
                {
                    TestContext.WriteLine($"Numbers NOT null");

                    foreach (var num in numbers)
                        TestContext.WriteLine($"num = {num};");
                }
                else
                {
                    TestContext.WriteLine($"Numbers is NULL");
                }
            }
        }

        private class ArrayOptionHandler : TestOptionHandler
        {
            [CliOptionProperty] public int[] Numbers { get; private set; }

            public override ICollection<int> GetNumbers() => Numbers;
        }

        private class ListOptionHandler : TestOptionHandler
        {
            [CliOptionProperty] public List<int> Numbers { get; private set; }

            public override ICollection<int> GetNumbers() => Numbers;
        }

        private class HashSetOptionHandler : TestOptionHandler
        {
            [CliOptionProperty] public HashSet<int> Numbers { get; private set; }

            public override ICollection<int> GetNumbers() => Numbers;
        }
    }
}
